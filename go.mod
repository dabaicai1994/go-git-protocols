module go-git-protocols

go 1.14

require (
	github.com/anmitsu/go-shlex v0.0.0-20200514113438-38f4b401e2be // indirect
	github.com/creack/pty v1.1.11 // indirect
	github.com/gliderlabs/ssh v0.3.1
	github.com/gorilla/mux v1.8.0
	github.com/kr/pty v1.1.8 // indirect
	golang.org/x/crypto v0.0.0-20201117144127-c1f2f97bffc9
)
